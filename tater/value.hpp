#pragma once

#include <tater/type.hpp>

#include <iostream>
#include <variant>

namespace tater
{
  class Function;

  /// Integer values are 64 bits in size and signed by default.
  using Int_value = std::int64_t;

  /// Floating point values are IEEE 754 double precision.
  using Float_value = double;

  /// Addresses are indexes into program memory.
  using Addr_value = std::uint64_t;

  /// Function values are pointers to functions.
  using Fn_value = Function*;

  /// Stores one of any value.
  ///
  /// \todo The arguments must correspond to the types in value and in order.
  using Value_variant = std::variant<
    Int_value, 
    Float_value, 
    Addr_value,
    Fn_value
  >;


  /// Represents the value of an operand or object in memory.
  class Value : public Value_variant
  {
  public:
    explicit Value(int n);
    /// Constructs an integer value.

    explicit Value(Int_value n);
    /// Constructs an integer value.

    explicit Value(Float_value n);
    /// Constructs a floating point value.

    explicit Value(Addr_value n);
    /// Constructs an address value.

    explicit Value(Fn_value f);
    /// Constructs an address value.

    Type get_type() const { return (Type)index(); }
    /// Returns the kind of value.

    bool is_integer() const { return get_type() == int_type; }
    /// Returns true if this is an integer value.
    
    bool is_float() const { return get_type() == float_type; }
    /// Returns true if this is a floating point value.
    
    bool is_address() const { return get_type() == addr_type; }
    /// Returns true if this is an address value.

    bool is_function() const { return get_type() == fn_type; }
    /// Returns true if this is an function value.

    Int_value get_integer() const { return std::get<int_type>(*this); }
    /// Returns this as an integer value.

    std::int64_t get_signed() const { return (std::int64_t)get_integer(); }
    /// Returns this as a signed integer.

    std::uint64_t get_unsigned() const { return (std::uint64_t)get_integer(); }
    /// Returns this as an unsigned integer.
    
    Float_value get_float() const { return std::get<float_type>(*this); }
    /// Returns this as a floating point value.
    
    Addr_value get_address() const { return std::get<addr_type>(*this); }
    /// Returns this as an address.

    Fn_value get_function() const { return std::get<fn_type>(*this); }
    /// Returns this as a function.
  };

  inline
  Value::Value(int n)
    : Value_variant(Int_value(n))
  { }

  inline
  Value::Value(Int_value n)
    : Value_variant(n)
  { }

  inline
  Value::Value(Float_value n)
    : Value_variant(n)
  { }

  inline
  Value::Value(Addr_value n)
    : Value_variant(n)
  { }

  inline
  Value::Value(Fn_value f)
    : Value_variant(f)
  { }

  std::ostream& operator<<(std::ostream& os, Value const& v);

} // namespace tater
