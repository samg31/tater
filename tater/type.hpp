#pragma once

namespace tater
{
  /// Represents different types of values.
  ///
  /// \todo When we add functions to the IR, we'll need to include
  /// function type. In fact, we probably need to support pointer
  /// types so that we can correctly process pointers to ints and floats
  /// and other pointers.
  ///
  /// \todo Make this an AST.
  enum Type
  {
    int_type,
    float_type,
    addr_type,
    fn_type,
  };

} // naemspace tater

